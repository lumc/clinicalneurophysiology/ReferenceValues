#' ---
#' title: "Automatically generated reference tables"
#' author: "Robert Reijntjes"
#' date: '`r format(Sys.Date(), "%d %B, %Y")`'
#' tags: [ reference-values, mixture-model-clustering ]
#' output:
#'   html_document:
#'      toc: true
#'      highlight: zenburn
#'   md_document:
#'     pandoc_args: ["--wrap=none"]
#'     variant: gfm
#'     preserve_yaml: TRUE
#'   pdf_document:
#'     latex_engine: xelatex
#'     toc: true
#'     number_sections: true
#'     highlight: zenburn
#' ---

#+ include = F
library(zoo)
library(lmerTest)
library(emmeans)
library(pander)
library(officer)
library(flextable)
library(NcsNorms)
library(tidyverse)

panderOptions('digits', 2)
panderOptions('round', 2)
panderOptions('keep.trailing.zeros', TRUE)
options(Encoding="UTF-8")
knitr::opts_chunk$set(echo = FALSE, message=FALSE, warning=FALSE)
options(gtsummary.print_engine = "kable")

theme_set(theme_article())
projFolder=here::here()
rawFolder = file.path(projFolder,'data-raw')
dataFolder = file.path(projFolder,'data')
tblFolder = file.path(projFolder,'results/tables')
repFolder = file.path(projFolder,'results/reports')
scriptFolder = file.path(projFolder,'scripts/reports')

load(file.path(dataFolder,"mot_clusters_lm.rda"))

mot_norms = mot_clusters_lm |>
  mutate(useHeight=nerve=='tibial' | nerve == 'peroneal') |>
  group_by(nerve,recording_site,stimulation_site,unit,useHeight) |>
  group_modify(~autoStratification(.x,useHeight = .y$useHeight)) |>
  mutate_at(vars(perc3,mn2sd),~if_else(unit %in% c('amplitude','area'),10^.,.)) |>
  ungroup() |>
  select(nerve,recording_site,stimulation_site,unit,age,sex,height,everything(),-useHeight) |>
  mutate_at(vars(age,height),~renameRanges(.)) |>
  mutate(height=forcats::fct_relevel(height,"< 160", "160 - 169","< 170", "170 - 179", "≥ 170", "≥ 180")) |>
  mutate(age=forcats::fct_relevel(age,"< 40", "< 50", "< 60",
                                  "40 - 49", "40 - 59", "40 - 69", "50 - 59", "50 - 69", "60 - 69",
                                  "≥ 40", "≥ 50", "≥ 60", "≥ 70")) #|>
  # select(recording_site,nerve,`units of measure`=unit,age,everything())

refs = mot_norms |>
  select(nerve,`recording site`=recording_site,`units of measure`=unit,sex,height,age,everything(),-stimulation_site) |>
  arrange(nerve,`recording site`,`units of measure`,sex,height,age) |>
  mutate(n=as.integer(n)) |>
  droplevels()


makeArmsLegsTable <- function(.df) {
  .df = .df |>
    regulartable() |>
    merge_v(j=c("nerve","recording site")) |>
    merge_v(j=c("units of measure")) |>
    merge_v(j=c("sex")) |>
    merge_v(j=c("age")) |>
    merge_v(j=c("height")) |>
    set_header_labels(mn = "mean",
                      mn2sd = paste0("mean\U00B1","2sd"),
                      perc3="3rd percentile") |>
    align(align = "center", part = "header" ) |>
    align_nottext_col(align = "left") |>
    fontsize(part = "all", size = 10) |>
    set_formatter_type(fmt_double="%.01f") |>
    empty_blanks() |>
    theme_box() |>
    autofit()
}

refs_arm = refs |>
  filter(nerve=='median' | nerve=='ulnar') |>
  makeArmsLegsTable()

#' # Introduction
#'
#' ## How the tables were created
#'
#' To enable the stratification of many nerve conduction tests, we created an automatic stratification function for each nerve conduction test (NCT) and unit of measure (UOM). A simple linear model is created that tests which independent variables, (age, height and sex) have significant effects. If there is a significant difference in sex, a male and female partition is made. If age or height change significantly, the function determines which age and height partitions to create. The first age partition ranges from 19 to less than 40 years. After that there can be partitions of 10, 20 or 40 years. The last partition is for ages greater than or equal to 80. For body heights, the first partition is any length less than 160 cm. After that, there can be two partitions with a length of 10 cm or one of 20 cm. The last partition are body heights of 180 cm or higher.
#'
#' The age and height partitions are created in two steps:
#'
#' As a first step, we determined how much the UOM changes over certain age and height ranges. First, we calculated the standard deviation (SD) for all measurements of an UOM in a NCT. We set a percentage of this SD as the limit value. A low percentage would mean many small partitions and a high percentage just a few partitions. After experimenting we choose the default value of 30% of the SD. We then examined whether the UOM changed more than this limit value over certain age and height ranges. If not, the range was increased. Taking age as an example, we started with 10-year partitions. If the UOM did not change more than 30% of the total SD of that UOM in 10 years, the range was increased to 20 years. If that range did not change more than 30% of the SD of that UOM either, we then proceeded to make partitions of 40 years.
#'
#' In the second step we determined whether consecutive partitions were significantly different. If the measurements in two adjacent partitions (e.g., 40-49 and 50-59 years old) did not differ significantly, these two partitions were merged.
#'
#' ## Explanation of the tables
#'
#' Because a standard deviation created on the logarithmic scale can't be converted to the natural scale, the amplitude and area mean and SD variables in the tables are still on the logarithmic scale.
#'
#' # arms table
#'
#+ results='asis', echo=FALSE
refs_arm

refs_leg = refs |>
  filter(nerve=='peroneal' | nerve=='tibial') |>
  makeArmsLegsTable()

#' # legs table
#'
#+ results='asis', echo=FALSE
refs_leg


# The line below produces a markdown file in the same folder as this script file.
# The markdown folder is only used on the wiki pages of https://gitlab.com/lumc/clinicalneurophysiology/ReferenceValues
# rmarkdown::render("./scripts/reports/reportAmplitudeAreaTransformations.R",output_file="flexmixRefs_auto.pdf",output_format='pdf_document',encoding="UTF-8")
#
# The line below produces a html file in the results/reports folder
# rmarkdown::render("./scripts/reports/reportAutomaticTables.R", output_file="../../results/reports/flexmixRefs_auto.html",encoding="UTF-8")
