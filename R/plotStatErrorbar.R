##' plots a summary with error ribbon
##'
##' @param df the dataframe with the hrv summary data
##' @param varName the variable to be plotted
##' @param .xName the x-axis variable name
##' @param .group the grouping variable
##' @param .facet the facets variable
##' @param .shape the shape of the errorbar point
##' @param .useLine whether a line should be drawn between the errorbars
##' @param .dodge by default false. If true grouped errorbars are not at the same position vertically.
##' @param .useSE by default true. If true standard error is used for the ribbon area else standard deviation
##' @param meanStr if the column name of the mean values is different than xxx_mean you can give the mean name here
##' @param seStr if the column name of the se values is different than xxx_se you can give the se name here
##' @author Robert Reijntjes
##' @examples
##' \dontrun{
##' df=seSdDataframe(mtcars,c(cyl,am),mpg)
##' df$am=factor(df$am)
##' plotStatErrorbar(df,mpg_mean,mpg_se,.xAxis=cyl,.group=am,.dodge = 0.4)
##'
##' df=seSdDataframe(mtcars,c(cyl,am,vs),mpg)
##' plotStatErrorbar(df,mpg_mean,mpg_se,.xAxis=cyl,.group=interaction(vs,am),.dodge = 0.4)
##' }
##' @keywords plot, summary
##' @export
plotStatErrorbar<-function(df,varMean,varSeSd,.xAxis,.group=NULL,.shape=NULL, .useLine=FALSE,.dodge=NA,.size=4.0) {
  varMean=rlang::quo_squash(rlang::enquo(varMean))
  varName=rlang::quo_name(varMean)
  varSeSd=rlang::quo_squash(rlang::enquo(varSeSd))
  .xAxis=rlang::quo_squash(rlang::enquo(.xAxis))
  .xAxisName=rlang::quo_name(.xAxis)
  .shape=rlang::quo_squash(rlang::enquo(.shape))
  # if (!is.null(.group))
  # .groupTmp=rlang::enquos(.group)
  # if (length(.groupTmp)==1) {
    .group = rlang::quo_squash(rlang::enquo(.group))
  # } else {
  #   int1=rlang::quo_squash(.groupTmp[[1]][2][[1]][2][[1]])
  #   int2=rlang::quo_squash(.groupTmp[[1]][2][[1]][3][[1]])
  #   .group = rlang::quo(interaction(!!int1, !!int2))
  # }
  if (is.factor(df |> pull(!!.xAxis))==FALSE) {
    extents = df |> summarise(ext=max(!!.xAxis)-min(!!.xAxis)) |> pull(ext)
    bar_width = extents/75
  } else
    bar_width=0.5

  aesthetics=aes(y=!!varMean, x=!!.xAxis,group=!!.group,colour=!!.group,shape=!!.shape)
  limits <- aes(ymax = !!varMean+!!varSeSd, ymin=!!varMean-!!varSeSd,fill=!!.group)
  if (!is.na(.dodge))
    pos<-position_dodge(width =.dodge)
  else
    pos<-"identity"

  p<-ggplot(df,aesthetics) +
    geom_point(size=.size,position=pos) + scale_shape_manual(values=c(16, 15, 17, 3, 7, 8)) +
    scale_size_area() +
    geom_errorbar(limits, width = bar_width,position=pos,size=.size/4) +
#     scale_size_area() +
    xlab(.xAxisName) +
    ylab(varName)

  if (.useLine==TRUE)
    p=p + geom_line(size=.size/4,position=pos)
  p
}
